import * as assert from 'assert';

import { DefaultLanguageRegionDelimitersConfig, getRegionDelimiters } from '../common';

suite("Extension Tests", function () {
    test("Get proper language region delimiters from config", function() {
        const languageIdentifier = 'ruby';
        const regionDelimiter = getRegionDelimiters(languageIdentifier, DefaultLanguageRegionDelimitersConfig);

        assert.deepEqual(regionDelimiter, DefaultLanguageRegionDelimitersConfig[languageIdentifier]);    
    });

    test("Get default language region delimiters for files that don't have a value set", function() {
        const languageIdentifier = 'plaintext';
        const regionDelimiter = getRegionDelimiters(languageIdentifier, DefaultLanguageRegionDelimitersConfig);

        assert.deepEqual(regionDelimiter, DefaultLanguageRegionDelimitersConfig['default']);    
    });
});
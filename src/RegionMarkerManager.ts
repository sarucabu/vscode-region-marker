import * as vscode from 'vscode';

import { DefaultLanguageRegionDelimitersConfig, getRegionDelimiters, LanguageDelimitersConfig } from './common';


export class RegionMarkerManager {
    private languageDelimiters: LanguageDelimitersConfig = {};
    private disposables: vscode.Disposable[] = [];

    constructor() {
        this.disposables.push(
            vscode.workspace.onDidChangeConfiguration(this.handleDidChangeConfiguration.bind(this))
        );

        this.registerCommands();
        this.updateConfiguration();
    }

    registerCommands() {
        this.disposables.push(vscode.commands.registerCommand('region-marker.markRegion', this.markRegion.bind(this)));
        this.disposables.push(vscode.commands.registerCommand('region-marker.markNamedRegion', this.markNamedRegion.bind(this)));
    }

    handleDidChangeConfiguration(event: vscode.ConfigurationChangeEvent) {
        if (event.affectsConfiguration('region-marker.languages')) {
            this.updateConfiguration();
        }
    }

    initializeDelimitersConfig() {
        this.languageDelimiters = {...DefaultLanguageRegionDelimitersConfig };
    }

    updateConfiguration() {
        this.initializeDelimitersConfig();
        
        let languagesConfig = vscode.workspace.getConfiguration('region-marker', null).get('languages') as LanguageDelimitersConfig;
        if (languagesConfig) {
            Object.keys(languagesConfig).forEach(language => {
                this.languageDelimiters[language] = languagesConfig[language];
            });
        }
    }

    markRegion(name?: string) {
        let activeTextEditor = vscode.window.activeTextEditor as vscode.TextEditor;
        if (activeTextEditor) {
            const regionDelimiter = getRegionDelimiters(activeTextEditor.document.languageId, this.languageDelimiters);
            let initialRegionDelimiter = name ? `${regionDelimiter.start} ${name}` : regionDelimiter.start;

            activeTextEditor.edit((editBuilder) => {
                let firstValidLine = undefined;
                let lastValidLine = undefined;
                
                let primarySelection = activeTextEditor.selection;
                for (let i = primarySelection.start.line; i <= primarySelection.end.line; i++) {
                    const line = activeTextEditor.document.lineAt(i);
                    if (!line.isEmptyOrWhitespace) {
                        if (firstValidLine === undefined) {
                            firstValidLine = line;
                        }

                        lastValidLine = line;
                    }
                }

                if (firstValidLine && lastValidLine) {
                    const indentationRange = new vscode.Range(
                        firstValidLine.lineNumber,
                        0,
                        firstValidLine.lineNumber,
                        firstValidLine.firstNonWhitespaceCharacterIndex
                    );
                    const indentationText = activeTextEditor.document.getText(indentationRange);
                    editBuilder.insert(firstValidLine.range.start.with(undefined, 0),  `${indentationText}${initialRegionDelimiter}\n`);
                    editBuilder.insert(lastValidLine.range.end, `\n${indentationText}${regionDelimiter.end}`);
                }
            });
        }
    }

    async markNamedRegion() {
        const regionName = await vscode.window.showInputBox({
            prompt: 'Region name',
        });

        if (regionName) {
            this.markRegion(regionName);
        }
    }

    dispose() {
        this.disposables.forEach(disposable => disposable.dispose());
    }
}
'use strict';

import * as vscode from 'vscode';

import { RegionMarkerManager } from './RegionMarkerManager';

export function activate(context: vscode.ExtensionContext) {
    context.subscriptions.push(new RegionMarkerManager());
}

export function deactivate() {
    // no-op
}
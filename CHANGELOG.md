# Change Log
All notable changes to the "region-marker" extension will be documented in this file.

## [1.0.0]
- Initial release
- Provides `Region Marker: Mark Region` and `Region Marker: Mark Named Region` commands
- Support for customizing region delimiters via settings
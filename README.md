# Region Marker

Region Marker allows you define code regions quickly.

## Features

Mark code regions selecting a chunk of code and running the `Region Marker: Mark Region` command.
> Tip: Invoking the command with no selection will create a region on the cursor position line.

![Region Marker: Mark Region](images/mark-region.gif)

---

Also, you can mark regions of code with a name by running the `Region Marker: Mark Named Region` command.

![Region Marker: Mark Named Region](images/mark-named-region.gif)


### Out of the box supported languages
* JavaScript
* TypeScript
* Ruby
* C#
* C
* C++
* Powershell
* Visual Basic

> Support for other languages can be added by adding region limiters via settings `region-marker.languages`

## Extension Settings

This extension contributes the following settings:

* `region-marker.languages`: Customizes the region delimiters for languages

```
"region-marker.languages": {
     // Language id
    "typescript": {
        "start": "// my-custom-region ",
        "end": "// end-of-custom-region"
    }
}
```

Will make `TypeScript` regions to be like:

![Custom delimiter example](images/custom-delimiter.png)

